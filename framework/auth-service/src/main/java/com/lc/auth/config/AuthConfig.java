package com.lc.auth.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("lc.auth")
public class AuthConfig {
    private String key;
    private Long defaultExpiry;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Long getDefaultExpiry() {
        return defaultExpiry;
    }

    public void setDefaultExpiry(Long defaultExpiry) {
        this.defaultExpiry = defaultExpiry;
    }
}
