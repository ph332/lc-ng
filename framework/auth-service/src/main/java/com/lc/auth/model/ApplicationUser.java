package com.lc.auth.model;

import javax.persistence.*;

@Entity
@Table(name = "ezl_login_tbl")
public class ApplicationUser {

    @Column(name="PASSWORD")
    private String password; // Password

    @Column(name="USER_TYPE") // User Type (Customer / Owner / Driver)
    private String type;

    @Column(name="CONTACT_ID")
    private String contactId;

    @Column(name="FNAME")
    private String FName;

    @Column(name="LNAME")
    private String LName;


    @Column(name="USER_SOURCE")
    private String userSource;

    //	@Id
    @Column(name="USER_ID")
    private String userId;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="LOGIN_ID")
    private int loginId;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getFName() {
        return FName;
    }

    public void setFName(String FName) {
        this.FName = FName;
    }

    public String getLName() {
        return LName;
    }

    public void setLName(String LName) {
        this.LName = LName;
    }

    public String getUserSource() {
        return userSource;
    }

    public void setUserSource(String userSource) {
        this.userSource = userSource;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getLoginId() {
        return loginId;
    }

    public void setLoginId(int loginId) {
        this.loginId = loginId;
    }
}
