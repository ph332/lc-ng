package com.lc.auth.repo;

import com.lc.auth.model.ApplicationUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationUserRepo extends CrudRepository<ApplicationUser, String> {
    ApplicationUser findByUserId(String userId);
}
