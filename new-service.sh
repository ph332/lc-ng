#!/bin/bash

while getopts "s:a:" opt
do
   case "$opt" in
      a ) ARTEFACT="$OPTARG" ;;
      s ) SERVICE="$OPTARG" ;;
   esac
done

if [ -z "${ARTEFACT}" ];
then
  echo "Usage ./new-service.sh -a <ArtefactID> -s <ServiceName>";
  exit 1;
elif [ -z "${SERVICE}" ];
then
   echo "Usage ./new-service.sh -a <ArtefactID> -s <ServiceName>";
   exit 1;
else
  mvn install archetype:update-local-catalog -f tools/service-gen/pom.xml
  mvn archetype:generate -DarchetypeArtifactId=service-gen -DarchetypeGroupId=com.lc -DarchetypeVersion=1.0.0-SNAPSHOT -DinteractiveMode=false -DserviceName="${SERVICE}" -DarchetypeCatalog=local -DgroupId=com.lc -DartifactId="${ARTEFACT}" -Dversion=1.0.0-SNAPSHOT -DserviceArtefactName="${ARTEFACT}"
  git add "${ARTEFACT}"
fi