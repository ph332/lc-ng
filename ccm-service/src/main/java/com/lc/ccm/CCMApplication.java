package com.lc.ccm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class CCMApplication {

    public static void main(String[] args){
        SpringApplication.run(CCMApplication.class, args);
    }
}
